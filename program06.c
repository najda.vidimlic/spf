
// gcc program06.c -o program06 -lpthread
#define _GNU_SOURCE
#include <sched.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

void Thread1()
{
    //sleep(1);
    int i,j;
    int policy;
    int a = 0;
    int b = 0;
    int print = 1;
    struct sched_param param;
    pthread_getschedparam(pthread_self(),&policy,&param);

    struct timespec deadline;
    clock_gettime(CLOCK_MONOTONIC, &(deadline));
    if (print == 1){
	if(policy == SCHED_OTHER)
        printf("SCHED_OTHER\n");
   	if(policy == SCHED_RR)
        printf("SCHED_RR 1 \n");
    	if(policy==SCHED_FIFO)
         printf("SCHED_FIFO\n");
	print = 0;
    }
    
    while(1)
    {
	
	deadline.tv_nsec+=5000000;
	if(deadline.tv_nsec >= 1000000000) {
		deadline.tv_nsec -= 1000000000;
		deadline.tv_sec++;
	}
      clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &deadline, NULL);

         for(j=1;j<60000;j++)
        {
		a += 1;
		b += a * 2;
        }
	a = 0;
	b = 0;
    }

}


int main()
{
    int i;
    i = getuid();
    if(i==0)
        printf("The current user is root\n");
    else
        printf("The current user is not root\n");

    pthread_t ppid1;
    struct sched_param param;

    pthread_attr_t attr1;
    pthread_attr_init(&attr1);
    
    cpu_set_t cpu;
    CPU_ZERO(&cpu);
    CPU_SET(3,&cpu);
    pthread_attr_setaffinity_np(&attr1, sizeof(cpu_set_t),&cpu);

    param.sched_priority = 99;
    pthread_attr_setschedpolicy(&attr1,SCHED_FIFO);
    pthread_attr_setschedparam(&attr1,&param);
    pthread_attr_setinheritsched(&attr1,PTHREAD_EXPLICIT_SCHED);


    pthread_create(&ppid1,&attr1,(void *)Thread1,NULL);
    pthread_join(ppid1,NULL);

    pthread_attr_destroy(&attr1);
    return 0;
}
