import datetime
time = str(datetime.datetime.now().time())
path = 'C:/SFP/log_p06_1t_0423_1.txt'
data = open(path,'r')
writePath = 'C:/SFP/clean_log_file_program06_0423_1.txt'
txt_file = open(writePath,'w')
newLine = ""
append = False
while True:
    currentData = data.readline()
    for c in currentData:
        if c == ']':
            append = True
        if append == True:
            if c != ' ' and c != ']' and c != '.':
                newLine = newLine + c
    #newLine = newLine + '\0' # Termination not needed
    #print(type(newLine))
    
    if "program06" in newLine:
        #print(newLine)
        txt_file.write(newLine)
        
    append = False
    newLine = ""
    
    if currentData == "":
        txt_file.close()
        data.close()
        break


pathToSorted = 'C:/SFP/clean_log_file_program06_0423_1.txt'
sortedData = open(pathToSorted,'r')
timeFilePath = 'C:/SFP/times_program06_0423_1.txt'
timeFile = open(timeFilePath,'w')
headLine = "Wakeup Latency, Response Time, Execution Time, Period\n"
stringWake = ""
stringIn = ""
stringOut = ""
stringSecWake = ""
timeWake = 0
timeSwitchIn = 0
timeSwithOut = 0
timeSecondWake = 0
readLines = 0
latTime = 0
respTime = 0
exeTime = 0
period = 0
firstRead = True

timeFile.write(headLine)
while True:
    if firstRead == True:
        currentLine = sortedData.readline()
        for c in currentLine:
            if c == ':':
                break
            stringWake = stringWake + c
        timeWake = int(stringWake)
    currentLine = sortedData.readline()
    if currentLine == "":
        timeFile.close()
        sortedData.close()
        break
    readLines = readLines + 1
    for c in currentLine:
        if c == ':':
            break
        if readLines == 1:
            stringIn = stringIn + c
        elif readLines == 2:
            stringOut = stringOut + c
        elif readLines == 3:
            stringSecWake = stringSecWake + c
        

    if readLines == 3:
        readLines = 0
        
        timeSwitchIn = int(stringIn)
        timeSwitchOut = int(stringOut)
        timeSecondWake = int(stringSecWake)
        
        latTime = timeSwitchIn - timeWake
        respTime = timeSwitchOut - timeWake
        exeTime = timeSwitchOut - timeSwitchIn
        period = timeSecondWake - timeWake
        
        if firstRead == False:
            timeWake = timeSecondWake
        toWrite = str(latTime)+','+str(respTime)+','+str(exeTime)+','+str(period)+'\n'
        timeFile.write(toWrite)
        stringWake = ""
        stringIn = ""
        stringOut = ""
        stringSecWake = ""
                   
    firstRead = False  

    
    

        
